﻿using UnityEngine;

namespace Phase1
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float jumpforce = 7;
        [SerializeField] private float gravity = 14;
        [SerializeField] private float terminalVelocity = 20;
        [SerializeField] private float runSpeed = 5.0f;

        private CharacterController controller;
        private float verticalVelocity = 0;
        private Animator anim;

        private void Start()
        {
            controller = GetComponent<CharacterController>();
            anim = GetComponent<Animator>();
        }

        private void Update()
        {
            Vector3 input = PoolInput();
            Vector3 moveDelta = new Vector3(input.x,0,0) * runSpeed;

            if (controller.isGrounded)
            {
                verticalVelocity = -1;
                if (Input.GetKeyDown(KeyCode.Space) || input.y > 0)
                {
                    verticalVelocity = jumpforce;
                    anim.SetTrigger("Jump");
                }
            }
            else
            {
                verticalVelocity -= gravity * Time.deltaTime;
                if (verticalVelocity < -terminalVelocity)
                    verticalVelocity = -terminalVelocity;

                if(verticalVelocity < 0)
                    anim.SetTrigger("Fall");
            }

            anim.SetBool("Grounded", controller.isGrounded);
            anim.SetFloat("Speed", Mathf.Abs(moveDelta.x));

            moveDelta.y = verticalVelocity;
            controller.Move(moveDelta * Time.deltaTime);
        }

        private Vector3 PoolInput()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            return new Vector3(x, y, 0);
        }
    }
}
