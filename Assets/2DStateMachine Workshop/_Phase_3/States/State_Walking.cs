﻿using UnityEngine;

namespace Phase3
{
    public class State_Walking : BaseState
    {
        public float runSpeed = 7.0f;

        public override Vector3 ProcessMotion(Vector3 input)
        {
            // Keep our player on the floor with small force
            motor.verticalVelocity = -1;

            input *= runSpeed;
            input.y = motor.verticalVelocity;

            return input;
        }

        public override void Transition()
        {
            if (!motor.isGrounded)
                motor.ChangeState(GetComponent<State_Falling>());

            if (Input.GetKeyDown(KeyCode.Space) || motor.inputVector.y > 0)
                motor.ChangeState(GetComponent<State_Jumping>());

            if (Input.GetKeyDown(KeyCode.LeftShift))
                motor.ChangeState(GetComponent<State_Dash>());
        }
    }
}
