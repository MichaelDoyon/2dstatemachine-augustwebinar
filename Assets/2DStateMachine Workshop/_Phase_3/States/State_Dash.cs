﻿using UnityEngine;

namespace Phase3
{
    public class State_Dash : BaseState
    {
        public float dashDuration = 0.1f;
        public float dashSpeed = 30.0f;

        private bool right;
        private float startTime;

        public override void Construct()
        {
            startTime = Time.time;
            motor.verticalVelocity = 0;

            // Find out which direction we're facing with scale
            right = motor.transform.localScale.x > 0;
        }

        public override Vector3 ProcessMotion(Vector3 input)
        {
            input.x = (right) ? dashSpeed : -dashSpeed;
            //Don't apply gravity
            input.y = 0;

            return input;
        }

        public override void Transition()
        {
            if (Time.time - startTime > dashDuration)
                motor.ChangeState(GetComponent<State_Falling>());
        }
    }
}