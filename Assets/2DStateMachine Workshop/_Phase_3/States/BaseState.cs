﻿using UnityEngine;

namespace Phase3
{
    public abstract class BaseState : MonoBehaviour
    {
        // Refs
        protected PlayerMotor motor;

        private void Awake()
        {
            motor = GetComponent<PlayerMotor>();
        }

        public virtual void Construct()
        {

        }
        public virtual void Destruct()
        {

        }
        public virtual void Transition()
        {

        }
        public virtual Vector3 ProcessMotion(Vector3 input)
        {
            Debug.Log("Process Motion not implemented in " + this.ToString());
            return Vector3.zero;
        }
    }
}
