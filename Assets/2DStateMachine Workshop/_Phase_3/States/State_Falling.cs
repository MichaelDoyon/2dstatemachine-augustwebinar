﻿using UnityEngine;

namespace Phase3
{
    public class State_Falling : BaseState
    {
        public float airSpeedInfluence = 0.5f;

        public override void Construct()
        {
            motor.anim.SetTrigger("Fall");
        }

        public override Vector3 ProcessMotion(Vector3 input)
        {
            motor.verticalVelocity -= motor.gravity * Time.deltaTime;
            input *= motor.baseRunSpeed * airSpeedInfluence;

            // While we do modify our vertical velocity, we still have to apply it to our movement vector
            input.y = motor.verticalVelocity;

            return input;
        }

        public override void Transition()
        {
            if (motor.isGrounded)
                motor.ChangeState(GetComponent<State_Walking>());

            if (Input.GetKeyDown(KeyCode.LeftShift))
                motor.ChangeState(GetComponent<State_Dash>());
        }
    }
}
