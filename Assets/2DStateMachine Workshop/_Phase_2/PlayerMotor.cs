﻿using UnityEngine;

namespace Phase2
{
    public class PlayerMotor : MonoBehaviour
    {
        [HideInInspector] public Vector3 inputVector;
        [HideInInspector] public Vector3 moveVector;
        [HideInInspector] public float verticalVelocity;
        [HideInInspector] public bool isGrounded;

        public float baseRunSpeed = 5.0f;
        public float gravity = 14;
        public float terminalVelocity = 20;

        private BaseState state;
        private CharacterController controller;

        #region Start
        public virtual void Start()
        {
            controller = GetComponent<CharacterController>();

            // Initial state set on Walking
            state = GetComponent<State_Walking>();
            state.Construct();
        }
        #endregion

        #region Update
        private void Update()
        {
            UpdateMotor();
        }
        protected virtual void UpdateMotor()
        {
            // Look for inputs
            inputVector = PoolInput();
            moveVector.x = inputVector.x;

            // Check if we are grounded once per frame, and store it locally for states to use
            isGrounded = controller.isGrounded;

            // Ask Mobility state to Calculate Motion
            moveVector = state.ProcessMotion(moveVector);
            // Rotate sprite based off the direction
            if(moveVector.x != 0)
                transform.localScale = (moveVector.x > 0) ? new Vector3(1, 1, 1) : new Vector3(-1, 1, 1);

            // Check for StateTransitions
            state.Transition();

            // Move the Controller
            Move();
        }
        protected virtual Vector3 PoolInput()
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            return new Vector3(x, y, 0);
        }
        protected virtual void Move()
        {
            controller.Move(moveVector * Time.deltaTime);
        }
        #endregion

        public virtual void ChangeState(BaseState s)
        {
            state.Destruct();
            state = s;
            state.Construct();
        }
    }
}