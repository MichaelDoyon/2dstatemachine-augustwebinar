﻿using UnityEngine;

namespace Phase2
{
    public abstract class BaseState : MonoBehaviour
    {
        // Refs
        protected PlayerMotor motor;
        protected float startTime;

        private void Awake()
        {
            motor = GetComponent<PlayerMotor>();
        }

        public virtual void Construct()
        {
            startTime = Time.time;
        }
        public virtual void Destruct()
        {

        }
        public virtual void Transition()
        {

        }
        public virtual Vector3 ProcessMotion(Vector3 input)
        {
            Debug.Log("Process Motion not implemented in " + this.ToString());
            return Vector3.zero;
        }
    }
}
